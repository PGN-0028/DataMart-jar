﻿insert into tempo_dim
select
  cast(to_char(data,'yyyymmdd') as bigint) tempo_sk,
  EXTRACT(YEAR from data) ano,
  case
      when (EXTRACT(MONTH from data)) in (1,2,3) then 1
      when (EXTRACT(MONTH from data)) in (4,5,6) then 2
      when (EXTRACT(MONTH from data)) in (7,8,9) then 3
      when (EXTRACT(MONTH from data)) in (10,11,12) then 4
  end trimestre,
  case
      when (EXTRACT(MONTH from data)) in (1,2) then 1
      when (EXTRACT(MONTH from data)) in (3,4) then 2
      when (EXTRACT(MONTH from data)) in (5,6) then 3
      when (EXTRACT(MONTH from data)) in (7,8) then 4
      when (EXTRACT(MONTH from data)) in (9,10) then 5
      when (EXTRACT(MONTH from data)) in (11,12) then 6
  end bimestre,
  to_char(data,'TMMonth') nome_mes,
  to_char(data,'TMDay') nome_dia_semana
from (
  select '2013-01-01'::date + vl as data
  from generate_series(0,5000) vl 
) data_table
order by ano