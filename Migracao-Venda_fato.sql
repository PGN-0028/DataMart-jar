﻿insert into venda_fato
select
  produto_sk,
  filial_sk,
  tempo_sk,
  valor
from (
   select p.produto_sk,f.filial_sk,t.tempo_sk, sum(valor) valor
   from vendas v inner join produto_dim p on (p.produto = v.produto)
   inner join filial_dim f on (f.filial = v.filial)
   inner join tempo_dim t on (cast(to_char(v.data,'yyyymmdd') as bigint) = t.tempo_sk)
   group by 1,2,3
) data
order by 1,2,3  